let express = require('express');
let chat = require('./chat/index.js');
let app = express();
let router = express.Router();
let port = process.env.PORT || 3000;

let addPostWordFrequency = 0.25;

let stdPunct = [
    {pre: '', post: '.'},
    {pre: '¡', post: '!'}
];

let questionPunct = [
    {pre: '¿', post:'?'}
];

let allPunct = stdPunct.concat(questionPunct);

let postWords = [
    'papi',
    'on the spot',
    'pa'
];

let phrases = [
    {type: 'image', initial: false, image: 'jry.png'},
    {type: 'image', initial: false, image: 'commando_head.gif'},
    {type: 'image', initial: false, image: 'llama.gif'},
    {type: 'phrase', initial: false, phrase: 'Y dale con la manigueta', allowsPost: true, punct: stdPunct},
    {type: 'phrase', initial: false, phrase: 'Quitea la vida', allowsPost: true, punct: stdPunct, dismissal: true},
    {type: 'phrase', initial: false, phrase: 'Tu sistema fracasó', allowsPost: true, punct: stdPunct},
    {type: 'phrase', initial: false, phrase: 'La marcha de las dragas', allowsPost: true, punct: stdPunct},
    {type: 'phrase', initial: false, phrase: 'Alimenta el zafacón', allowsPost: true, punct: stdPunct},
    {type: 'phrase', initial: false, phrase: 'Me arruinaste la experiencia', allowsPost: true, punct: stdPunct},
    {type: 'phrase', initial: false, phrase: 'Quién... Crijto', allowsPost: false, punct: questionPunct},
    {type: 'phrase', initial: false, phrase: 'Me cago en los cojones del demonio', allowsPost: true, punct: stdPunct},
    {type: 'phrase', initial: false, phrase: 'Eso es una churretá', allowsPost: true, punct: stdPunct},
    {type: 'phrase', initial: false, phrase: 'Escreta pura', allowsPost: true, punct: stdPunct, dismissal: true},
    {type: 'phrase', initial: false, phrase: 'Me estás majando los cojones', allowsPost: true, punct: allPunct},
    {type: 'phrase', initial: false, phrase: 'Me lo estás majando', allowsPost: true, punct: allPunct},
    {type: 'phrase', initial: false, phrase: 'Me lo estás pulverizando', allowsPost: true, punct: stdPunct},
    {type: 'phrase', initial: false, phrase: 'Me lo estás mondando', allowsPost: true, punct: allPunct},
    {type: 'phrase', initial: false, phrase: "Me hiciste mofongo 'e bola", allowsPost: true, punct: [{pre: '', post: '...'}]},
    {type: 'phrase', initial: false, phrase: 'Váyase al carajo', allowsPost: true, punct: stdPunct, dismissal: true},
    {type: 'phrase', initial: false, phrase: 'JAAAALOOOU', allowsPost: false, punct: [{pre: '¡', post: '!'}]},
    {type: 'phrase', initial: false, phrase: "Paaara el jueve'", allowsPost: false, punct: [{pre: '', post: '...'}]},
    {type: 'phrase', initial: false, phrase: 'Háblate con Jerry', allowsPost: false, punct: [{pre: '', post: '...'}]},
    {type: 'phrase', initial: false, phrase: 'Le preguntaste a Monclavo', allowsPost: false, punct: questionPunct},
    {type: 'phrase', initial: false, phrase: 'Pregúntale a Monclavo', allowsPost: false, punct: [{pre: '', post: '.'}, {pre: '', post: '...'}]},
    {type: 'phrase', initial: false, phrase: 'Bah', allowsPost: false, punct: stdPunct, dismissal: true},
    {type: 'phrase', initial: false, phrase: 'What da hell', allowsPost: false, punct: [{pre: '', post: '...'}, {pre: '', post: '?!'}]},
    {type: 'phrase', initial: false, phrase: 'What da fuck is going on', allowsPost: false, punct: [{pre: '', post: '...'}, {pre: '', post: '?!'}]},
    {type: 'phrase', initial: false, phrase: 'Mondaste', allowsPost: true, punct: stdPunct, dismissal: true},
    {type: 'phrase', initial: false, phrase: 'Me cago en la chicha', allowsPost: false, punct: stdPunct},
    {type: 'phrase', initial: false, phrase: "No tengo tiempo 'pa eso", allowsPost: false, punct: stdPunct},
    {type: 'phrase', initial: false, phrase: 'Perdiste el tiempo', allowsPost: true, punct: stdPunct},
    {type: 'phrase', initial: false, phrase: "Que clase 'e mondadura", allowsPost: false, punct: stdPunct},
    {type: 'phrase', initial: false, phrase: 'No vengas a mondar', allowsPost: false, punct: stdPunct},
    {type: 'phrase', initial: false, phrase: 'Oh my god... help me', allowsPost: false, punct: [{pre: '', post: '!'}]},
    {type: 'phrase', initial: false, phrase: 'A mi no me vengas con ese truco, papi', allowsPost: false, punct: [{pre: '¡', post: '!'}]},
    {type: 'phrase', initial: false, phrase: "Fuck 'em", allowsPost: false, punct: [{pre: '', post: '.'}]},
    {type: 'phrase', initial: false, phrase: 'Ay ay ay ay', allowsPost: false, punct: [{pre: '¡', post: '!'}]},
    {type: 'phrase', initial: false, phrase: 'Flochea', allowsPost: true, punct: stdPunct},
    {type: 'phrase', initial: false, phrase: 'Me lo pico y me lo como', allowsPost: false, punct: stdPunct},
    {type: 'phrase', initial: false, phrase: 'Me lo pico y me lo como... lo cago... y me lo vuelvo a comer', allowsPost: false, punct: stdPunct},
    {type: 'phrase', initial: false, phrase: 'Se te fue el avión', allowsPost: true, punct: stdPunct, dismissal: true},
    {type: 'phrase', initial: false, phrase: 'Game over', allowsPost: true, punct: [{pre: '', post: '.'}, {pre: '', post: '...'}], dismissal: true},
    {type: 'phrase', initial: false, phrase: 'Se me está cayendo la bolsa', allowsPost: true, punct: stdPunct},
    {type: 'phrase', initial: false, phrase: 'Zafacón', allowsPost: false, punct: stdPunct, dismissal: true},
    {type: 'phrase', initial: false, phrase: 'Qué pinga', allowsPost: false, punct: questionPunct},
    {type: 'phrase', initial: false, phrase: 'Qué bueno', allowsPost: false, punct: stdPunct},
    {type: 'phrase', initial: false, phrase: 'Me lo rebanaste completamente, rebotó, y se me pegó otra vez', allowsPost: false, punct: stdPunct},
    {type: 'phrase', initial: false, phrase: 'Unbelievable', allowsPost: false, punct: [{pre: '', post: '!'}, {pre: '', post: '...'}]},
    {type: 'phrase', initial: false, phrase: 'Un-fucking-believable', allowsPost: false, punct: [{pre: '', post: '!'}, {pre: '', post: '...'}]},

    {type: 'phrase', initial: true, phrase: 'Estás haciendo piragua', allowsPost: true, punct: allPunct},
    {type: 'phrase', initial: true, phrase: 'Fracasaste', allowsPost: true, punct: allPunct},
    {type: 'phrase', initial: true, phrase: 'Te fracasó el sistema', allowsPost: true, punct: allPunct},
    {type: 'phrase', initial: true, phrase: 'No tienes sistema', allowsPost: true, punct: allPunct},
    {type: 'phrase', initial: true, phrase: 'Te fuiste a la mierda', allowsPost: true, punct: allPunct},
    {type: 'phrase', initial: true, phrase: 'Te fuiste a oblivión', allowsPost: true, punct: allPunct},
    {type: 'phrase', initial: true, phrase: 'Estás repartiendo salami', allowsPost: true, punct: allPunct},
    {type: 'phrase', initial: true, phrase: 'Estás repartiendo salami como frisbee', allowsPost: true, punct: allPunct},
    {type: 'phrase', initial: true, phrase: 'Dime algo interesante', allowsPost: true, punct: [{pre: '', post: '...'}], overrideInitialPunct: true},
    {type: 'phrase', initial: true, phrase: 'Estás comiéndote la mierda', allowsPost: true, punct: stdPunct},
    {type: 'phrase', initial: true, phrase: 'Mere cabrón', allowsPost: false, punct: stdPunct, overrideInitialPunct: true},
    {type: 'phrase', initial: true, phrase: 'Ayúdame, por favor', allowsPost: false, punct: stdPunct, overrideInitialPunct: true},
    {type: 'phrase', initial: true, phrase: 'Me estás trolliando', allowsPost: false, punct: questionPunct},
    {type: 'phrase', initial: true, phrase: 'Estás fabricando', allowsPost: true, punct: allPunct},
    ];

let initiationPhrases = phrases.filter(p => p.initial).map(p => ({phrase: p.phrase, allowsPost: p.allowsPost, punct: p.overrideInitialPunct ? p.punct : questionPunct}))
    .concat([{phrase: 'Qué pajó, papi', allowsPost: false, punct: questionPunct}]);

let dismissalPhrases = phrases.filter(p => !!p.dismissal);

randomPhrase = (phraseList) => {
    let r = Math.floor(Math.random() * phraseList.length);
    return phraseList[r];
};

randomPostWord = (postWordList, enabled) => {
    if (enabled && Math.random() < addPostWordFrequency) {
        let r = Math.floor(Math.random() * postWordList.length);
        return `, ${postWordList[r]}`;
    } else {
        return '';
    }
};

randomPunct = (punct) => {
    let r = Math.floor(Math.random() * punct.length);
    return punct[r];
};

getArturoResponse = (question) => {
    let phrase = randomPhrase(phrases);
    if (phrase.type === 'image') {
        return {you: question, arturo: {img: phrase.image}};
    } else {
        let postWord = randomPostWord(postWords, phrase.allowsPost);
        let punct = randomPunct(phrase.punct);
        let a = `${punct.pre}${phrase.phrase}${postWord}${punct.post}`;
        return {you: question, arturo: a};
    }
};

getArturoInitiation = () => {
    let phrase = randomPhrase(initiationPhrases);
    if (phrase.type === 'image') {
        return {arturo: {img: phrase.image}};
    } else {
        let postWord = randomPostWord(postWords, phrase.allowsPost);
        let punct = randomPunct(phrase.punct);
        let a = `${punct.pre}${phrase.phrase}${postWord}${punct.post}`;
        return {arturo: a};
    }
};

getArturoDismissal = () => {
    let phrase = randomPhrase(dismissalPhrases);
    if (phrase.type === 'image') {
        return {arturo: {img: phrase.image}};
    } else {
        let postWord = randomPostWord(postWords, phrase.allowsPost);
        let punct = randomPunct(phrase.punct);
        let a = `${punct.pre}${phrase.phrase}${postWord}${punct.post}`;
        return {arturo: a};
    }
};

router.get('/ask/:question', function (req, res, next) {
    res.send(getArturoResponse(req.params.question))
});

router.get('/initiate', function (req, res, next) {
    res.send(getArturoInitiation());
});

router.get('/dismiss', function (req, res, next) {
    res.send(getArturoDismissal());
});

app.use('/api', router);
app.use('/chat', chat);
app.use(express.static('public'));
app.use('/',  function (req, res, next) {
    res.redirect('/chat');
});
app.listen(port, () => console.log(`Arturobot is listening on port ${port}!`));
