let express = require('express');
let path = require('path');
let router = express.Router();

router.get('/', function (req, res) {
    res.sendFile('index.html', { root: path.join(__dirname, '../chat') })
});

module.exports = router;